
2. Commentaire :
a. Commentez l’entête de chaque fonction. Ces commentaires doivent contenir la description de la fonction et le rôle de ces paramètres.
b. Une ligne contient soit un commentaire, soit du code, pas les deux.
c. Utilisez des noms d’identificateur significatifs.
d. Placez vos noms dans un commentaire au début du code.
3. Code :
a. Pas de goto.
b. Un seul return par méthode.
4. Indentez votre code.
a. Assurez-vous que l’indentation est faite avec des espaces.
aGauche( Noeud t, clef k )
si t == null alors resultat := – ∞
sinon si k < t.clef alors aGauche( t.gauche, k )
sinon max( t.gauche.a, t.valeur, aGauche( t.droite, k ) )
4
b. Pas de ligne de plus de 80 caractères.